package acessando_atributos_de_objetos;

public class Principal {

	
	public static void main(String[] args) {
		
		Carro meuCarro = new Carro();
		
		meuCarro.fabricante = "Fiat";
		meuCarro.modelo ="Palio";
		meuCarro.anoDeFabricacao = 2011;
		meuCarro.cor ="Prata";
		
		System.out.println("Meu carro");
		System.out.println("--------------");
		System.out.println("\n" + "Modelo " + meuCarro.modelo +
				"\n" + "Ano " + meuCarro.anoDeFabricacao);
		
		
		Carro seuCarro = new Carro();
		
		seuCarro.anoDeFabricacao = 1978;
		seuCarro.cor  = "Vermelho";
		seuCarro.fabricante = "Volkswagen";
		seuCarro.modelo = "Brazilia";
		
		
		System.out.println("\n\n\nSeu carro");
		System.out.println("--------------");
		System.out.println("Modelo " + seuCarro.modelo);
		System.out.println("\n Cor " + seuCarro.cor);
		System.out.println("\n Ano de fabricação " +seuCarro.anoDeFabricacao);
		
		
		
		

	}

}
