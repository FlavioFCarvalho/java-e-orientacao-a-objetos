package string_stringBuilder_stringBuffer;

public class TesteStrings {

	
	public static void main(String[] args) {
		
		//String, StringBuilder e StringBuffer
		
		String s = "Olá";
		s = s + " Pessoal";
		
		System.out.println(s+"\n");
		
		StringBuilder sb = new StringBuilder("Olá");//Existe a StringBuilder
		sb.append("Pessoal");//Reapoveitando a StringBuilder
		
		String resultado = sb.toString();
		System.out.println("Com StringBuilder: " + resultado);

	}

}
